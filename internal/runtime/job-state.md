```mermaid
---
title: containerd job states
---
stateDiagram-v2
  [*] --> Created
  Created --> Started
  Created --> Failed
  Started --> Paused
  Started --> Succeed
  Started --> Failed
  Paused --> Resumed
  Paused --> Failed
  Resumed --> Paused
  Resumed --> Succeed
  Resumed --> Failed
  Succeed --> [*]
  Failed --> [*]
```
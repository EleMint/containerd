package runtime

import "fmt"

type JobState string

const (
	JOB_NIL     JobState = "nil"
	JOB_CREATED JobState = "created"
	JOB_STARTED JobState = "started"
	JOB_PAUSED  JobState = "paused"
	JOB_RESUMED JobState = "resumed"
	JOB_SUCCEED JobState = "succeed"
	JOB_FAILED  JobState = "failed"
)

func transerr(a, b JobState) error {
	return fmt.Errorf("cannot transition from %s to %s", a, b)
}

func (curr JobState) ValidateNextState(next JobState) error {
	if curr == next {
		return fmt.Errorf("connot transition to the same state %s", curr)
	}

	err := transerr(curr, next)

	switch curr {
	case JOB_NIL:
		if next == JOB_CREATED || next == JOB_FAILED {
			return nil
		}
		return err
	case JOB_CREATED:
		if next == JOB_STARTED || next == JOB_FAILED {
			return nil
		}
		return err
	case JOB_STARTED:
		if next == JOB_PAUSED || next == JOB_SUCCEED || next == JOB_FAILED {
			return nil
		}
		return err
	case JOB_PAUSED:
		if next == JOB_RESUMED || next == JOB_FAILED {
			return nil
		}
		return err
	case JOB_RESUMED:
		if next == JOB_PAUSED || next == JOB_FAILED {
			return nil
		}
		return err
	case JOB_SUCCEED:
		return err
	case JOB_FAILED:
		return err
	default:
		return fmt.Errorf("unknown job state %v", curr)
	}
}

package runtime

import (
	"bytes"
	"containerd/internal/git"
	"containerd/pkg/prefixer"
	"context"
	stderr "errors"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/pkg/stdcopy"
	specs "github.com/opencontainers/image-spec/specs-go/v1"
	"github.com/pkg/errors"
)

type JobID string

func (id JobID) String() string {
	return string(id)
}

func (id JobID) Comp(other string) bool {
	return id.String() == other
}

func (id JobID) Short() string {
	return string(id)[:8]
}

type Job struct {
	rt *Runtime
	p  *Pipeline

	ID JobID

	Name    string
	Stage   string
	Image   string
	Scripts [][]string
	Script  string
	RepoDir string
	Needs   []string
	State   JobState
}

func (rt *Runtime) NewJob(conf ConfigJob) *Job {
	j := &Job{
		rt: rt,
		p:  nil,

		ID:      "",
		Name:    conf.Name,
		Stage:   conf.Stage,
		Image:   conf.Image,
		Scripts: conf.Scripts,
		Needs:   conf.Needs,

		State: JOB_NIL,
	}
	return j
}

func (j *Job) updateState(next JobState) {
	j.State = next
}

func (j *Job) ensureState(states ...JobState) error {
	for _, state := range states {
		if j.State == state {
			return nil
		}
	}

	return fmt.Errorf(
		"could not match job state; current state %s; expected one of %v",
		j.State,
		states,
	)
}

func (j *Job) loadRepo(url string) error {
	repoDir, err := git.Clone(fmt.Sprintf("%s-%s-*", j.p.ID.Short(), j.Name), url)
	if err != nil {
		return errors.Wrap(err, "could not clone repo into temp directory")
	}

	j.RepoDir = repoDir

	return nil
}

func (j *Job) createScript() error {
	f, err := os.CreateTemp("", fmt.Sprintf("*-%s", j.Name))
	if err != nil {
		return errors.Wrap(err, "could not create temp script file")
	}

	j.Script = f.Name()

	err = f.Chmod(0777)
	if err != nil {
		return errors.Wrap(err, "could not make temp script file executable")
	}

	script := strings.Builder{}
	script.WriteString("#!/bin/sh\n\n")

	for i, s := range j.Scripts {
		for _, arg := range s {
			script.WriteString(arg)
			script.WriteString(" ")
		}

		if i != len(j.Scripts)-1 {
			script.WriteString(" && ")
		}
	}

	script.WriteString("\n")

	n, err := f.WriteString(script.String())
	if err != nil {
		return errors.Wrap(err, "could not write script to temp file")
	}

	if n != script.Len() {
		return fmt.Errorf(
			"could not write entire script to temp file; expected %d; wrote %d",
			script.Len(),
			n,
		)
	}

	err = f.Close()
	if err != nil {
		return errors.Wrap(err, "could not flush temp script file")
	}

	return nil
}

func (j *Job) Create(ctx context.Context) error {
	err := j.State.ValidateNextState(JOB_CREATED)
	if err != nil {
		return err
	}
	defer j.updateState(JOB_CREATED)

	const entry = "/var/opt/containerd/init"

	workdir := "/containerd"

	containerConfig := &container.Config{
		Image:      j.Image,
		WorkingDir: workdir,
		User:       "root:root",
		Tty:        false,
		Entrypoint: []string{entry},
		Labels: map[string]string{
			"runtime":  "containerd",
			"pipeline": j.p.ID.String(),
			"name":     j.Name,
		},
	}

	err = j.createScript()
	if err != nil {
		return errors.Wrap(err, "could not create job script")
	}

	err = j.loadRepo(j.p.URL)
	if err != nil {
		return errors.Wrap(err, "could not load repo for container host config mount")
	}

	hostConfig := &container.HostConfig{
		Privileged: true,
		LogConfig:  container.LogConfig{},
		RestartPolicy: container.RestartPolicy{
			Name: "no",
		},
		Mounts: []mount.Mount{
			{
				Type:   mount.TypeBind,
				Source: j.Script,
				Target: entry,
			},
			{
				Type:   mount.TypeBind,
				Source: j.RepoDir,
				Target: workdir,
			},
		},
	}

	networkConfig := &network.NetworkingConfig{}

	platform := &specs.Platform{}

	containerName := fmt.Sprintf("%s-%s", j.Name, j.p.ID.Short())

	resp, err := j.rt.cli.ContainerCreate(
		ctx,
		containerConfig,
		hostConfig,
		networkConfig,
		platform,
		containerName,
	)
	if len(resp.Warnings) > 0 {
		fmt.Fprintf(os.Stderr, "[WARN][JOB] Container Create")
		for _, warning := range resp.Warnings {
			fmt.Fprintf(os.Stderr, "[WARN][JOB]\t- %s\n", warning)
		}

		var e error
		if err != nil {
			e = errors.Wrapf(
				err,
				"container create response contained %d warnings",
				len(resp.Warnings),
			)
		} else {
			e = fmt.Errorf("container create response contained %d warnings", len(resp.Warnings))
		}
		return e
	}
	if err != nil {
		return errors.Wrapf(err, "could not create container %s", resp.ID)
	}

	j.ID = JobID(resp.ID)

	return nil
}

func (j *Job) Start(ctx context.Context) error {
	err := j.State.ValidateNextState(JOB_STARTED)
	if err != nil {
		return err
	}
	defer j.updateState(JOB_STARTED)

	startOptions := types.ContainerStartOptions{}

	err = j.rt.cli.ContainerStart(ctx, j.ID.String(), startOptions)
	if err != nil {
		return errors.Wrapf(err, "could not start container %s", j.ID)
	}

	return nil
}

func (j *Job) Wait(ctx context.Context) (<-chan container.WaitResponse, <-chan error) {
	wr, err := j.rt.cli.ContainerWait(ctx, j.ID.String(), container.WaitConditionNotRunning)
	return wr, err
}

type JobResult struct {
	ID   string
	Name string
	Logs *JobLogs
	Err  error
}

func (j *Job) WaitAndReport(ctx context.Context, wg *sync.WaitGroup, res chan<- *JobResult) {
	defer wg.Done()

	result := &JobResult{
		ID:   j.ID.String(),
		Name: j.Name,
	}

	respCh, errCh := j.Wait(ctx)
	select {
	case err := <-errCh:
		if err != nil {
			result.Err = err
		}
	case status := <-respCh:
		if status.Error != nil {
			result.Err = fmt.Errorf(status.Error.Message)
			fmt.Printf("[WARN] job exited with status code %d\n", status.StatusCode)
			fmt.Printf("[WARN] %s\n", status.Error)
		} else {
			fmt.Printf("[INFO] job exited with status code %d\n", status.StatusCode)
		}
	}

	logs, err := j.Logs(ctx)
	if err != nil {
		if result.Err != nil {
			result.Err = stderr.Join(
				result.Err,
				errors.Wrapf(err, "could not get job logs %s", j.Name),
			)
		}
	}

	if logs != nil {
		result.Logs = logs
	}

	res <- result
}

type JobLogs struct {
	JobID   JobID
	JobName string
	Stdout  *bytes.Buffer
	Stderr  *bytes.Buffer
	Stdcmb  *bytes.Buffer
}

func newLogs() *JobLogs {
	return &JobLogs{
		Stdout: &bytes.Buffer{},
		Stderr: &bytes.Buffer{},
		Stdcmb: &bytes.Buffer{},
	}
}

func (j *Job) Logs(ctx context.Context) (logs *JobLogs, err error) {
	err = j.ensureState(
		JOB_STARTED,
		JOB_PAUSED,
		JOB_RESUMED,
		JOB_SUCCEED,
		JOB_FAILED,
	)
	if err != nil {
		return
	}

	logOptions := types.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: true,
		Timestamps: true,
		Details:    true,
	}

	out, err := j.rt.cli.ContainerLogs(ctx, j.ID.String(), logOptions)
	if err != nil {
		err = errors.Wrapf(err, "could not get container log stdout reader %s", j.ID)
		return
	}

	logs = newLogs()

	preStdout := prefixer.New(logs.Stdout, logs.Stdcmb, "[STDOUT]")
	preStderr := prefixer.New(logs.Stderr, logs.Stdcmb, "[STDERR]")

	n, err := stdcopy.StdCopy(preStdout, preStderr, out)
	if err != nil && err != io.EOF {
		err = errors.Wrapf(err, "could not read container logs (%d bytes)", n)
		return
	}

	logs.JobID = j.ID
	logs.JobName = j.Name

	err = nil

	return
}

func (j *Job) Cleanup(ctx context.Context) error {
	err := j.rt.cli.ContainerRemove(ctx, j.ID.String(), types.ContainerRemoveOptions{
		RemoveVolumes: true,
		Force:         true,
	})
	if err != nil {
		return errors.Wrapf(err, "could not remove job container %s", j.ID)
	}

	err = os.RemoveAll(j.RepoDir)
	if err != nil {
		return errors.Wrapf(err, "could not remove job temp repo %s", j.RepoDir)
	}

	return nil
}

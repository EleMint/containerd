package runtime

import (
	stderr "errors"
	"fmt"
	"strings"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

type ConfigJob struct {
	Name    string
	Stage   string
	Image   string
	Extends []string
	Scripts [][]string
	Needs   []string
}

type ConfigJobKey string

const (
	KEY_STAGE   ConfigJobKey = ".stage"
	KEY_IMAGE   ConfigJobKey = ".image"
	KEY_SCRIPTS ConfigJobKey = ".scripts"
	KEY_NEEDS   ConfigJobKey = ".needs"
)

func (cjk ConfigJobKey) Match(name string, path string) bool {
	return path == fmt.Sprintf("%s%s", name, cjk)
}

func splitArgs(scripts []string) [][]string {
	res := [][]string{}

	for _, script := range scripts {
		// buf := []string{}

		// echo "deploying app..."

		// var args []string
		//
		// for {
		// 	args = strings.SplitN(script, " \t", 1)
		// 	if strings.ContainsAny(args[0], "\"'") {
		//
		// 	}
		// }

		args := strings.SplitN(script, " ", 1)

		res = append(res, args)
	}

	return res
}

func newConfigJob(name string, paths []string) (cj ConfigJob, errs []error) {
	cj.Name = name

	for _, path := range paths {
		if KEY_STAGE.Match(name, path) {
			cj.Stage = viper.GetString(path)
		} else if KEY_IMAGE.Match(name, path) {
			cj.Image = viper.GetString(path)
		} else if KEY_SCRIPTS.Match(name, path) {
			scripts := viper.GetStringSlice(path)
			cj.Scripts = splitArgs(scripts)
		} else if KEY_NEEDS.Match(name, path) {
			cj.Needs = viper.GetStringSlice(path)
		}
	}

	if cj.Image == "" {
		errs = append(errs, fmt.Errorf("job %s must define runtime image", name))
	}
	if len(cj.Scripts) == 0 {
		errs = append(errs, fmt.Errorf("job %s must define scripts", name))
	}

	return
}

type Config struct {
	Repo    string
	Include []string
	Stages  []string
	Jobs    []ConfigJob
}

type ConfigKey string

const (
	KEY_REPO    ConfigKey = "repo"
	KEY_INCLUDE ConfigKey = "include"
	KEY_STAGES  ConfigKey = "stages"
)

func (ck ConfigKey) String() string {
	return string(ck)
}

func getJobName(path string) string {
	return strings.Split(path, ".")[0]
}

func getConfigJobMap(keys []string) map[string][]string {
	jobMap := map[string][]string{}

	for _, key := range keys {
		if key == "repo" || key == "include" || key == "stages" {
			continue
		}

		if strings.HasPrefix(key, ".") {
			continue
		}

		// valid job

		name := getJobName(key)
		found, ok := jobMap[name]
		if !ok {
			jobMap[name] = []string{key}
		} else if len(found) > 0 {
			jobMap[name] = append(jobMap[name], key)
		}
	}

	return jobMap
}

func ParseConfig(configFile string) (c Config, errs []error) {
	viper.SetConfigFile(configFile)

	err := viper.ReadInConfig()
	if err != nil {
		errs = append(errs, errors.Wrapf(err, "failed to read config file %s", configFile))
		return
	}

	c.Repo = viper.GetString(KEY_REPO.String())
	c.Include = viper.GetStringSlice(KEY_INCLUDE.String())
	c.Stages = viper.GetStringSlice(KEY_STAGES.String())
	c.Jobs = []ConfigJob{}

	keys := viper.AllKeys()
	jobMap := getConfigJobMap(keys)

	for jobName, jobPaths := range jobMap {
		j, cErrs := newConfigJob(jobName, jobPaths)
		if cErrs != nil && len(cErrs) > 0 {
			errs = append(
				errs,
				errors.Wrapf(stderr.Join(cErrs...), "could not parse job %s", jobName),
			)
			return
		}

		c.Jobs = append(c.Jobs, j)
	}

	// sort.Slice(keys, func(i, j int) bool {
	// 	return keys[i] < keys[j]
	// })

	// fmt.Println("Job Keys:")
	// for _, key := range keys {
	// 	fmt.Printf("\t- %s\n", key)
	// }

	return
}

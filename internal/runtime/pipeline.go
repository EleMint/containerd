package runtime

import (
	"context"
	stderr "errors"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
)

type PipelineID string

func (id PipelineID) String() string {
	return string(id)
}

func (id PipelineID) Comp(other string) bool {
	return id.String() == other
}

func (id PipelineID) Short() string {
	return string(id)[:8]
}

type PipelineState string

const (
	PIPELINE_NIL     PipelineState = "nil"
	PIPELINE_STARTED PipelineState = "started"
)

type Pipeline struct {
	rt  *Runtime
	URL string

	ID    PipelineID
	State PipelineState
	Jobs  []*Job

	// jobs    map[string]*Job
	created map[string]bool
	started map[string]bool
	done    map[string]bool
}

func (rt *Runtime) NewPipeline(configPath string) (*Pipeline, error) {
	conf, errs := ParseConfig(configPath)
	if errs != nil && len(errs) > 0 {
		return nil, errors.Wrap(stderr.Join(errs...), "could not parse pipeline configuration")
	}

	id := randStringBytesMaskImprSrcSB(64)

	p := &Pipeline{
		rt:  rt,
		URL: conf.Repo,

		ID:    PipelineID(id),
		State: "",
		Jobs:  []*Job{},
		// jobs:    make(map[string]*Job, len(jobs)),
		created: map[string]bool{},
		started: map[string]bool{},
		done:    map[string]bool{},
	}

	p.AddJobs(conf.Jobs...)

	return p, nil
}

func (p *Pipeline) AddJob(job *Job) {
	job.p = p
	p.Jobs = append(p.Jobs, job)
}

func (p *Pipeline) AddJobs(jobConfigs ...ConfigJob) {
	for _, conf := range jobConfigs {
		j := p.rt.NewJob(conf)
		p.AddJob(j)
	}
}

func (p *Pipeline) findJobById(id string) (found *Job) {
	found = nil
	for _, job := range p.Jobs {
		if job.ID == JobID(id) {
			found = job
			return
		}
	}
	return
}

func (p *Pipeline) findByName(name string) (found *Job) {
	found = nil
	for _, job := range p.Jobs {
		if job.Name == name {
			found = job
			return
		}
	}
	return
}

func (p *Pipeline) Start(ctx context.Context) (map[string]*JobLogs, map[string]error) {
	p.State = PIPELINE_STARTED

	errMap := map[string]error{}

	for _, j := range p.Jobs {
		// Create the Job if no needs
		if len(j.Needs) == 0 {
			err := j.Create(ctx)
			if err != nil {
				errMap[j.Name] = errors.Wrap(err, "could not create job")
				continue
			}
			p.created[j.Name] = true
		}
	}

	if len(errMap) > 0 {
		return nil, errMap
	}

	logMap := map[string]*JobLogs{}
	wg := sync.WaitGroup{}
	waitCh := make(chan bool, 1)
	resCh := make(chan *JobResult, len(p.created))

	go func(waitCh <-chan bool, resCh <-chan *JobResult, done map[string]bool, logMap map[string]*JobLogs, errMap map[string]error) {
		for {
			select {
			case res := <-resCh:
				if res.Err != nil {
					errMap[res.Name] = res.Err
				}
				if res.Logs != nil {
					logMap[res.Name] = res.Logs
				}
				done[res.Name] = true
				break
			case <-waitCh:
				return
			}
		}
	}(
		waitCh,
		resCh,
		p.done,
		logMap,
		errMap,
	)

	for k, v := range p.created {
		if v == false {
			errMap[k] = fmt.Errorf("cannot start non-created job %s", k)
			p.started[k] = false
			continue
		}

		j := p.findByName(k)
		if j == nil {
			errMap[k] = fmt.Errorf("cannot find job %s", k)
			p.started[k] = false
			continue
		}

		err := j.Start(ctx)
		if err != nil {
			errMap[j.Name] = errors.Wrapf(err, "could not start job %s", j.Name)
			p.started[j.Name] = false
			continue
		}

		fmt.Printf("[INFO] started job %s %s\n", j.Name, j.ID)
		p.started[j.Name] = true

		wg.Add(1)
		go j.WaitAndReport(ctx, &wg, resCh)
	}

	wg.Wait()
	waitCh <- true
	return logMap, errMap
}

func (p *Pipeline) Cleanup(ctx context.Context) error {
	var errs error

	for name := range p.done {
		job := p.findByName(name)

		err := os.Remove(job.Script)
		if err != nil {
			errs = stderr.Join(
				errs,
				errors.Wrapf(err, "could not remove job script file %s %s", job.Script, job.ID),
			)
			continue
		}

		err = job.Cleanup(ctx)
		if err != nil {
			errs = stderr.Join(errs, errors.Wrapf(err, "could not cleanup job %s", job.ID))
			continue
		}
	}

	return errs
}

var src rand.Source

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

func randStringBytesMaskImprSrcSB(n int) string {
	src = rand.NewSource(time.Now().UnixNano())

	sb := strings.Builder{}
	sb.Grow(n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			sb.WriteByte(letterBytes[idx])
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return sb.String()
}

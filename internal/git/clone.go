package git

import (
	"os"

	g "github.com/go-git/go-git/v5"
	"github.com/pkg/errors"
)

func Clone(tmpPattern string, url string) (string, error) {
	dir, err := os.MkdirTemp("", tmpPattern)
	if err != nil {
		return "", errors.Wrapf(err, "could not make temp directory from pattern %s", tmpPattern)
	}

	_, err = g.PlainClone(
		dir, false,
		&g.CloneOptions{
			URL:               url,
			RecurseSubmodules: g.DefaultSubmoduleRecursionDepth,
			Progress:          os.Stdout,
		},
	)
	if err != nil {
		return "", errors.Wrapf(err, "could not clone from url %s", url)
	}

	return dir, nil
}

package prefixer

import (
	"bytes"
	"io"

	"github.com/pkg/errors"
)

type Prefixer struct {
	prefix         string
	passthrough    io.ReadWriter
	accum          io.ReadWriter
	trailingNewine bool
	lineBuffer     bytes.Buffer
}

func New(passthrough io.ReadWriter, accum io.ReadWriter, prefix string) *Prefixer {
	return &Prefixer{
		prefix:         prefix,
		passthrough:    passthrough,
		accum:          accum,
		trailingNewine: true,
	}
}

func (pre *Prefixer) Write(p []byte) (n int, err error) {
	n, err = pre.passthrough.Write(p)
	if err != nil {
		return
	}

	pre.lineBuffer.Reset()

	for _, b := range p {
		if pre.trailingNewine {
			pre.lineBuffer.WriteString(pre.prefix)
			pre.trailingNewine = false
		}

		pre.lineBuffer.WriteByte(b)

		if b == '\n' {
			pre.trailingNewine = true
		}
	}

	n, err = pre.accum.Write(pre.lineBuffer.Bytes())
	if err != nil {
		if n > len(p) {
			n = len(p)
		}

		err = errors.Wrap(err, "could not write to underlying writer")

		return
	}

	n = len(p)

	return
}
